# Minecraft AFK Fishing

Automate Minecraft AFK Fishing for Linux. The act of Minecraft AFK (away from
keyboard) Fishing is fishing at a specially built fishing farm without
physically having to be present after initial setup.

* **Note:** Tested using Minecraft Java Edition v1.14.x and v1.15.x.
* **Note:** Tested on Arch Linux using Gnome v3.34.4 and v3.36.0 with Wayland
disabled.

Follow the below steps in order.

1. [Installation Instructions](#installation-instructions)
1. [Create The AFK Fishing Farm](#create-the-afk-fishing-farm)
1. [How To AFK Fish](#how-to-afk-fish)

# System Requirements

- Gawk v5.0+
- GNU Linux operating System
- GNU BASH v5.0+
- Xdotool v3.20160805+
- X Window without Wayland
- Xorg-xprop v1.2.4+

# Installation Instructions

**Assumptions**

- Have experienced working knowledge within a Linux CLI (command-line interface).
- Installation instructions below are done using Arch Linux via the CLI.
- Steps prefixed with a "$" (dollar sign) represents the CLI prompt. The text
after the "$" is to be entered at the CLI.
- Steps prefixed with a "#" (number sign) represents the CLI prompt with
elevated user permissions (e.g. root). The text after the "#" is to be entered
at the CLI.
- These instructions are an example of installation and configuration.

1. Open the terminal.
1. Install required packages.
   ```
   # pacman -Sy gawk xdotool xorg-xprop
   ```
1. Disable "pause on lost focus" for the Minecraft client.
   ```
   $ nano /home/username-here/.minecraft/options.txt
   ```
   Before
   ```
   pauseOnLostFocus:true
   ```
   After
   ```
   pauseOnLostFocus:false
   ```
   **Note:** Multimc "options.txt" example location: "/home/username-here/.minecraft/options.txt, /home/username-here/.local/share/multimc/instances/instance-name-here/.minecraft/options.txt".
1. Change to a desired directory (e.g. ~/Downloads).
   ```
   $ cd ~/Downloads
   ```
1. Clone the Minecraft AFK Fishing project.
   ```
   git clone https://gitlab.com/adouglas/minecraft-afk-fishing
   ```
1. Copy automation script.
   ```
   # cp minecraft-afk-fishing/minecraft-afk-fishing /usr/local/bin/
   ```
1. Set permissions.
   ```
   # chmod 755 /usr/local/bin/minecraft-afk-fishing
   ```
1. Set ownership.
   ```
   # chown root:root /usr/local/bin/minecraft-afk-fishing
   ```
1. Open Gnome settings (gnome-control-center) > Keyboard Shortcuts.

   **Note**: In Gnome v3.34.4 or older go to Gnome settings > Devices > Keyboard Shortcuts.
1. Add custom shortcut by scrolling to the bottom and left mouse click on the
plus (+) button.
1. At the "Set Custom Shortcut" dialog enter the following values.
   ```
   Name: Minecraft AFK Fishing Activate
   Command: /usr/local/bin/minecraft-afk-fishing activate 3
   ```
1. Left mouse click on "Set Shortcut..." button. Use desired shortcut (e.g. super + /).
1. Close the "Set Custom Shortcut" dialog.
1. Add custom shortcut, scroll to the bottom and left mouse click on the plus (+) button.
1. At the "Set Custom Shortcut" dialog enter the following values.
   ```
   Name: Minecraft AFK Fishing Deactivate
   Command: /usr/local/bin/minecraft-afk-fishing deactivate 3
   ```
1. Left mouse click on "Set Shortcut..." button. Use desired shortcut (e.g. shift + super + ?).
Close the "Set Custom Shortcut" dialog.
1. Close the Gnome settings (gnome-control-center) window.

## Create The AFK Fishing Farm

## Minecraft Materials

- Note block x 1
- Chest x 2
- Hopper x 1
- Fence x 1
- Pressure plate x 1
- Iron trapdoor x 1
- Bucket of water x 1
- Cobblestone x 1

## Build Instructions

1. Launch Minecraft.
1. Dig into the ground creating a 1 x 4 dug out area.
1. In the hole at the start put the chests down into one large chest.
1. Place the hopper beside it.
1. Place the fence on to the hopper.
1. Place the pressure plate onto the fence.
1. At the 2nd block away from hopper place the note block above ground. This
means there will be a hole between the position of the note block and the
hopper.
1. Place the cobblestone block onto the note block. This ensures the note block
is quite.
1. Place the water onto the fence.

# How To AFK Fish

1. Activate by pressing the custom shortcut key(s).
1. Aim the crosshair at the top of the iron trapdoor so that you are hitting
the note block. This will cause the fishing rod to repeatedly cast.
1. When desired, deactivate by pressing the custom shortcut key(s).

# Resources

- [Minecraft 1.14 AFK Fishing Farm Tutorial (Clicker Included) For The Village And Pillage Update](https://youtu.be/hTAHK2XnpQs) by xisumavoid
- [Minecraft 1.15 AFK Fishing Farm Tutorial For The Buzzy Bees Update](https://youtu.be/-wKW0OovGK4) by xisumavoid
- [Using an AFK Fish Farm in the background with xdotool on Linux](https://www.reddit.com/r/Minecraft/comments/6fb6cp/using_an_afk_fish_farm_in_the_background_with/) by u/Drirr

# License

Minecraft AFK Fishing is licensed under the GNU General Public License v3.0.
Review the license by viewing the LICENSE file.

# Copyright Notice

Copyright (c) 2019-2020 Adam Douglas Some Rights Reserved
